#!/usr/bin/env bash

QUI=$(whoami)

echo $QUI
if [ $QUI == "root" ]
	then 
		echo "[...] update database [...]"
		apt-get update > /dev/null
		echo "[...] upgrade system  [...]"
		apt-get upgrade > /dev/null
	else 
		echo "[/!\] Vous devez être super-utilisateur [/!\]"
fi

echo "Script terminé !!"

