#!/usr/bin/env bash

echo "[...] Checking internet connection [...]"
	ping -c3 8.8.8.8 > /dev/null
	TEST=$?

if [ $TEST -ne 0 ]
	then
		echo "[/!\] Not connected to Internet [/!\]"
		echo "[/!\] Please check configuration [/!\]"	
	else

		echo "[...] Internet access OK [...]"
		exit 0
fi

echo "Script terminer"
