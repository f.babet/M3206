function[] =Babet_Code_parti2_v()
format long
function[S] =fonc(n)                                                                                        %Ici on déclare notre fonction
        S= (4*(((-1)^(n)) * (((x1)^((2*n)+1))/((2*n)+1)))) - (((-1)^(n))*(((x2)^((2*n)+1))/((2*n)+1)));     %Ici On entre ce que représente le calcul de pi/4 avec arctan
end

x1 = 1/5;                                                                                                   %Première valeur nécessaire au calcul de pi/4 
x2 = 1/239;                                                                                                 %Deuxième valeur nécessaire au calcul de pi/4
n = 0;                                                                                                      %Numéro du rang au quel la série débute
eps = 10^(-30);                                                                                             
somme1 = 0;
while abs((4*(((x1)^((2*n)+1))/((2*n)+1))) - ((((x2)^((2*n)+1))/((2*n)+1)))) > eps                          %Condition d'arrêt = valeur absolue de notre fonction
    
    somme1 = somme1 + fonc(n);                                                                              %Permet de calculer la somme et n prendra la de n dans la fonction déclaré 
    n = n+1;                                                                                                %Permet d'augmenter le rang de la série                                                                                       
    
    
end
somme1                                                                                                      %Affiche le résutat de la somme
n                                                                                                           %Affiche le nombre de fois que la boucle a tourné
end