#!/usr/bin/env bash
nom=$(date +%Y_%m_%d_%H%M_tah)
echo $nom
read chemin
echo "Entrer le chemin du répertoire que vous souhaitez sauvegarder"
echo $chemin
compr=$nom.tar.gz
echo $compr
tar zcvf $compr $chemin
mv $compr /tmp/backup
