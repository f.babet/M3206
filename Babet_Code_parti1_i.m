function[] =Babet_Code_parti1_i()
format long
function[S] =fonc(x)                    %Ici on déclare notre fonction
        S= ((-1)^(x) * log(x)) / x;     %Ici On entre ce que représente notre fonction donc Sm
end

m = 1;					%Iniatilisation de m car la série commence à 1 
somme1 = 0;				
eps = 10^(-5);				%Valeur d'Epsilon, permet de changer la précision du calcule, et donc du nombre de boucle du while

					%La boucle while ci-dessous permet le calcul d'une somme
while abs(log(m+1)/(m+1)) > eps		%Ici entre entre la condition d'arrêt de la boucle donc |S(m+1) - Sm| < Eps
    
    somme1 = somme1 + fonc(m);		%Permet de calculer la somme et m prendra la de x dans la fonction déclaré ci-dessus	
    m = m+1;				%Permet d'augmenter le rang de la série
    
end
somme1					% Affiche le résultat de la somme 
end