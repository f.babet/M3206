function[] =Babet_Code_parti2_iii()
format long
function[S] =fonc(n)                                    %Ici on déclare notre fonction
        S= ((-1)^(n)) * (((x)^((2*n)+1))/((2*n)+1));    %Ici On entre ce que représente notre fonction donc arctan
end

x = input('Entrer la valeure de x: ');                  %Ici la personne pourra vérifier la valeur de x qu'il souhaite 
n = 0;                                                  %La série commence à 0
eps = 10^(-5);                                          %Valeur d'Epsilon, permet de changer la précision du calcule, et donc du nombre de boucle du while
somme1 = 0;

                                                        %La boucle while ci-dessous permet le calcul d'une somme
while abs((((x)^((2*n)+1))/((2*n)+1))) > eps            %Ici notre condition d'arrêt est la valeur absolue de notre fonction
    
    somme1 = somme1 + fonc(n);                          %Permet de calculer la somme et n prendra la de n dans la fonction déclaré ci-dessus
    n = n+1;                                            %Permet d'augmenter le rang de la série
    
    
end
somme1                                                  %Affiche le résulat de la somme
n                                                       %Compteur qui permet de savoir le nombre de fois que la boucle a tourné
end